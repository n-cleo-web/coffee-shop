var navButton = document.getElementById('navbtn')

function openMenu(e) {
    if (e.type == 'touchstart') {
        e.preventDefault();
    }

    const nav = document.getElementById('nav')
    const footer = document.getElementById('footer') 
    nav.classList.toggle('active')
    footer.classList.toggle('active')

    if (navButton.src.match("Assets/icon-navbar.jpg")) {
        navButton.src = "Assets/xnavbar.png"
    } else {
        navButton.src = "Assets/icon-navbar.jpg"
    }  
}

navButton.addEventListener('click', openMenu)
navButton.addEventListener('touchstart', openMenu)