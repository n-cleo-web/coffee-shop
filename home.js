const btnAddress = document.getElementById("btn-address")

function showMore(e) {
    const more = document.getElementById('show-more')
    const contacts = document.getElementById('contacts')
    if (e.type == 'touchstart') {
        e.preventDefault()
    }

    more.classList.toggle('active')
    contacts.classList.toggle('active')
}

btnAddress.addEventListener('click', showMore)
btnAddress.addEventListener('touchstart', showMore)